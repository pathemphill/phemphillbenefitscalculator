﻿using Moq;
using NUnit.Framework;
using PHemphillBenefitsCalculator.Controllers;
using PHemphillBenefitsCalculator.Models;
using PHemphillBenefitsCalculator.Services;
using System.Collections.Generic;

namespace PHemphillBenefitsCalculator.Tests.Controllers
{
    public class BenefitsPreviewControllerTest
    {
        private BenefitsPreviewController _controller;
        private Mock<ICostPreviewService> _mockCostPreviewService;
        private BenefitsCostPreviewModel _expectedModel;

        [SetUp]
        public void SetUp()
        {
            _expectedModel = new BenefitsCostPreviewModel();
            _mockCostPreviewService = new Mock<ICostPreviewService>(MockBehavior.Strict);
            _mockCostPreviewService.Setup(x => x.CalculateBenefitsPreview(It.IsAny<string>(), It.IsAny<List<string>>()))
                .Returns(_expectedModel)
                .Verifiable();

            _controller = new BenefitsPreviewController(_mockCostPreviewService.Object);
        }

        [Test]
        public void TestGet()
        {
            var result = _controller.Get("Bob", new List<string>(new string[] { "Alice", "Doris" }));

            Assert.AreEqual(_expectedModel, result.Value);
            _mockCostPreviewService.Verify();
        }
    }
}
