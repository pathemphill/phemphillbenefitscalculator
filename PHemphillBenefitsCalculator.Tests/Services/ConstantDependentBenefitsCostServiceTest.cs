﻿using Moq;
using NUnit.Framework;
using PHemphillBenefitsCalculator.Services;

namespace PHemphillBenefitsCalculator.Tests.Services
{
    public class ConstantDependentBenefitsCostServiceTest
    {
        private ConstantDependentBenefitsCostService _service;
        private Mock<IPayPeriodsService> _mockPayPeriodsService;
        private const double expectedCost = 500.0 / 26.0;

        [SetUp]
        public void SetUp()
        {
            _mockPayPeriodsService = new Mock<IPayPeriodsService>(MockBehavior.Strict);
            _mockPayPeriodsService.Setup(x => x.GetPayPeriodsPerYear())
                .Returns(26)
                .Verifiable();
            _service = new ConstantDependentBenefitsCostService(_mockPayPeriodsService.Object);
        }

        [Test]
        public void TestGetDependentBenefitsCostPerPayPeriod()
        {
            var result = _service.GetDependentBenefitsCostPerPayPeriod("anything");
            Assert.AreEqual(expectedCost, result, 0.00001);
            _mockPayPeriodsService.Verify();
        }
    }
}
