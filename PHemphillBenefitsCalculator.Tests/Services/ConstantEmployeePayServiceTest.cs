﻿using NUnit.Framework;
using PHemphillBenefitsCalculator.Services;

namespace PHemphillBenefitsCalculator.Tests.Services
{
    public class ConstantEmployeePayServiceTest
    {
        private ConstantEmployeePayService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new ConstantEmployeePayService();
        }

        [Test]
        public void TestGetEmployeePay()
        {
            Assert.AreEqual(2000.0, _service.GetEmployeePay("anything"), 0.0001);
        }
    }
}
