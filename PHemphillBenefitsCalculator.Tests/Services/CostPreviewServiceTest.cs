﻿using Moq;
using NUnit.Framework;
using PHemphillBenefitsCalculator.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PHemphillBenefitsCalculator.Tests.Services
{
    public class CostPreviewServiceTest
    {
        private CostPreviewService _service;
        private Mock<IEmployeePayService> _mockEmployeePayService;
        private Mock<IEmployeeBenefitsCostService> _mockEmployeeBenefitsCostService;
        private Mock<IEmployeeBenefitsDiscountService> _mockEmployeeBenefitsDiscountService;
        private Mock<IDependentBenefitsCostService> _mockDependentBenefitsCostService;
        private Mock<IDependentBenefitsDiscountService> _mockDependentBenefitsDiscountService;

        private string _employee;
        private List<string> _dependents;

        [SetUp]
        public void SetUp()
        {
            _mockEmployeePayService = new Mock<IEmployeePayService>();
            _mockEmployeePayService.Setup(x => x.GetEmployeePay(It.IsAny<string>()))
                .Returns(1300.0)
                .Verifiable();

            _mockEmployeeBenefitsCostService = new Mock<IEmployeeBenefitsCostService>();
            _mockEmployeeBenefitsCostService.Setup(x => x.GetEmployeeBenefitsCostPerPayPeriod(It.IsAny<string>()))
                .Returns(48.65)
                .Verifiable();

            _mockEmployeeBenefitsDiscountService = new Mock<IEmployeeBenefitsDiscountService>();
            _mockEmployeeBenefitsDiscountService.Setup(x => x.GetEmployeeBenefitsDiscount(It.IsAny<string>(), It.IsAny<double>()))
                .Returns(4.87)
                .Verifiable();

            _mockDependentBenefitsCostService = new Mock<IDependentBenefitsCostService>();
            _mockDependentBenefitsCostService.Setup(x => x.GetDependentBenefitsCostPerPayPeriod(It.IsAny<string>()))
                .Returns(35.22)
                .Verifiable();

            _mockDependentBenefitsDiscountService = new Mock<IDependentBenefitsDiscountService>();
            _mockDependentBenefitsDiscountService.Setup(x => x.GetDependentBenefitsDiscount(It.IsAny<string>(), It.IsAny<double>()))
                .Returns(3.52)
                .Verifiable();

            _service = new CostPreviewService(_mockEmployeePayService.Object,
                _mockEmployeeBenefitsCostService.Object,
                _mockEmployeeBenefitsDiscountService.Object,
                _mockDependentBenefitsCostService.Object,
                _mockDependentBenefitsDiscountService.Object);

            _employee = "Roger Wilco";
            _dependents = new List<string>(new string[] { "Ariel Wilco", "Wilbur Wilco"});
        }

        [Test]
        public void TestCalculateBenefitsPreview_EmployeeEchoed()
        {
            var result = _service.CalculateBenefitsPreview(_employee, _dependents);

            Assert.AreEqual(_employee, result.Employee);
        }

        [Test]
        public void TestCalculateBenefitsPreview_BasePay()
        {
            var result = _service.CalculateBenefitsPreview(_employee, _dependents);

            Assert.AreEqual(1300.0, result.BasePay, 0.0001);
            _mockEmployeePayService.Verify();
        }

        [Test]
        public void TestCalculateBenefitsPreview_EmployeeBenefitsCost()
        {
            var result = _service.CalculateBenefitsPreview(_employee, _dependents);

            Assert.AreEqual(43.78, result.EmployeeBenefitsCost, 0.0001);
            _mockEmployeeBenefitsCostService.Verify();
            _mockEmployeeBenefitsDiscountService.Verify();
        }

        [Test]
        public void TestCalculateBenefitsPreview_EmployeeDiscount()
        {
            var result = _service.CalculateBenefitsPreview(_employee, _dependents);

            Assert.AreEqual(4.87, result.EmployeeDiscount, 0.0001);
            _mockEmployeeBenefitsCostService.Verify();
            _mockEmployeeBenefitsDiscountService.Verify();
        }

        [Test]
        public void TestCalculateBenefitsPreview_NullDependents()
        {
            var result = _service.CalculateBenefitsPreview(_employee, null);

            Assert.AreEqual(43.78, result.BenefitsCostTotal, 0.0001);
            Assert.AreEqual(1256.22, result.PayAfterBenefits, 0.0001);
            Assert.IsNotNull(result.DependentCostPreviews);
            Assert.AreEqual(0, result.DependentCostPreviews.Count);
            _mockDependentBenefitsCostService.VerifyNoOtherCalls();
            _mockDependentBenefitsDiscountService.VerifyNoOtherCalls();
        }

        [Test]
        public void TestCalculateBenefitsPreview_NoDependents()
        {
            var result = _service.CalculateBenefitsPreview(_employee, new List<string>());

            Assert.AreEqual(43.78, result.BenefitsCostTotal, 0.0001);
            Assert.AreEqual(1256.22, result.PayAfterBenefits, 0.0001);
            Assert.IsNotNull(result.DependentCostPreviews);
            Assert.AreEqual(0, result.DependentCostPreviews.Count);
            _mockDependentBenefitsCostService.VerifyNoOtherCalls();
            _mockDependentBenefitsDiscountService.VerifyNoOtherCalls();
        }

        [Test]
        public void TestCalculateBenefitsPreview_WithDependents()
        {
            var result = _service.CalculateBenefitsPreview(_employee, _dependents);

            Assert.AreEqual(107.18, result.BenefitsCostTotal, 0.0001);
            Assert.AreEqual(1192.82, result.PayAfterBenefits, 0.0001);
            Assert.IsNotNull(result.DependentCostPreviews);
            Assert.AreEqual(2, result.DependentCostPreviews.Count);

            for (int i = 0; i < 2; ++i)
            {
                Assert.AreEqual(_dependents[i], result.DependentCostPreviews[i].Dependent);
                Assert.AreEqual(31.7, result.DependentCostPreviews[i].DependentBenefitsCost, 0.0001);
                Assert.AreEqual(3.52, result.DependentCostPreviews[i].DependentDiscount, 0.0001);
            }
        }
    }
}
