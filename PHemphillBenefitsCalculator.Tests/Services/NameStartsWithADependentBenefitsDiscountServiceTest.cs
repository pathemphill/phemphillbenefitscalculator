﻿using NUnit.Framework;
using PHemphillBenefitsCalculator.Services;

namespace PHemphillBenefitsCalculator.Tests.Services
{
    public class NameStartsWithADependentBenefitsDiscountServiceTest
    {
        private NameStartsWithADependentBenefitsDiscountService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new NameStartsWithADependentBenefitsDiscountService();
        }

        [Test]
        public void TestGetDependentBenefitsDiscount_NameStartsWithA_Discounted()
        {
            var result = _service.GetDependentBenefitsDiscount("Angela", 50.0);

            Assert.AreEqual(5.0, result, 0.00001);
        }

        [Test]
        public void TestGetDependentBenefitsDiscount_NameStartsWithE_NotDiscounted()
        {
            var result = _service.GetDependentBenefitsDiscount("Eric", 50.0);

            Assert.AreEqual(0.0, result, 0.00001);
        }
    }
}
