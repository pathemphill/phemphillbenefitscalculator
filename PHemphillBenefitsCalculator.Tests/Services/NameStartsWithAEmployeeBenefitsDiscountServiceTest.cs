﻿using NUnit.Framework;
using PHemphillBenefitsCalculator.Services;

namespace PHemphillBenefitsCalculator.Tests.Services
{
    public class NameStartsWithAEmployeeBenefitsDiscountServiceTest
    {
        private NameStartsWithAEmployeeBenefitsDiscountService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new NameStartsWithAEmployeeBenefitsDiscountService();
        }

        [Test]
        public void TestGetEmployeeBenefitsDiscount_NameStartsWithA_Discounted()
        {
            var result = _service.GetEmployeeBenefitsDiscount("Alicia", 63.5);

            Assert.AreEqual(6.35, result, 0.00001);
        }

        [Test]
        public void TestGetEmployeeBenefitsDiscount_NameStartsWithE_NotDiscounted()
        {
            var result = _service.GetEmployeeBenefitsDiscount("Eric", 12.3);

            Assert.AreEqual(0.0, result, 0.00001);
        }
    }
}
