﻿using NUnit.Framework;
using PHemphillBenefitsCalculator.Services;

namespace PHemphillBenefitsCalculator.Tests.Services
{
    public class TwoWeeksPayPeriodsServiceTest
    {
        private TwoWeeksPayPeriodsService _service;

        [SetUp]
        public void SetUp()
        {
            _service = new TwoWeeksPayPeriodsService();
        }

        [Test]
        public void TestGetPayPeriodsPerYear()
        {
            Assert.AreEqual(26, _service.GetPayPeriodsPerYear());
        }
    }
}
