﻿using Microsoft.AspNetCore.Mvc;
using PHemphillBenefitsCalculator.Models;
using PHemphillBenefitsCalculator.Services;
using System.Collections.Generic;

namespace PHemphillBenefitsCalculator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BenefitsPreviewController : ControllerBase
    {
        private readonly ICostPreviewService _costPreviewService;
        
        public BenefitsPreviewController(ICostPreviewService costPreviewService)
        {
            _costPreviewService = costPreviewService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<BenefitsCostPreviewModel> Get([FromQuery]string employee, [FromQuery]List<string> dependents)
        {
            return _costPreviewService.CalculateBenefitsPreview(employee, dependents);
        }
    }
}
