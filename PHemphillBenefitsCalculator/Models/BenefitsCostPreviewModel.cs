﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PHemphillBenefitsCalculator.Models
{
    public class BenefitsCostPreviewModel
    {
        public string Employee { get; set; }
        public double BasePay { get; set; }
        public double EmployeeDiscount { get; set; }
        public double EmployeeBenefitsCost { get; set; }
        public double BenefitsCostTotal { get; set; }
        public double PayAfterBenefits { get; set; }
        public List<DependentCostPreviewModel> DependentCostPreviews;
    }
}
