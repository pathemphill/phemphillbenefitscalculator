﻿namespace PHemphillBenefitsCalculator.Models
{
    public class DependentCostPreviewModel
    {
        public string Dependent { get; set; }
        public double DependentDiscount { get; set; }
        public double DependentBenefitsCost { get; set; }
    }
}
