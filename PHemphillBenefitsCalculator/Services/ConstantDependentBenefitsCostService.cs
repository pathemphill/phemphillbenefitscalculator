﻿namespace PHemphillBenefitsCalculator.Services
{
    public class ConstantDependentBenefitsCostService: IDependentBenefitsCostService
    {
        private readonly IPayPeriodsService _payPeriodsService;

        public ConstantDependentBenefitsCostService(IPayPeriodsService payPeriodsService)
        {
            _payPeriodsService = payPeriodsService;
        }

        public double GetDependentBenefitsCostPerPayPeriod(string dependent)
        {
            return 500.0 / (double)_payPeriodsService.GetPayPeriodsPerYear();
        }
    }
}
