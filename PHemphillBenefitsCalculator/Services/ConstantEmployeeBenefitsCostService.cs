﻿namespace PHemphillBenefitsCalculator.Services
{
    public class ConstantEmployeeBenefitsCostService : IEmployeeBenefitsCostService
    {
        private readonly IPayPeriodsService _payPeriodsService;

        public ConstantEmployeeBenefitsCostService(IPayPeriodsService payPeriodsService)
        {
            _payPeriodsService = payPeriodsService;
        }

        public double GetEmployeeBenefitsCostPerPayPeriod(string employee)
        {
            return 1000.0 / (double)_payPeriodsService.GetPayPeriodsPerYear();
        }
    }
}
