﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PHemphillBenefitsCalculator.Models;

namespace PHemphillBenefitsCalculator.Services
{
    public class CostPreviewService : ICostPreviewService
    {
        private readonly IEmployeePayService _employeePayService;
        private readonly IEmployeeBenefitsCostService _employeeBenefitsCostService;
        private readonly IEmployeeBenefitsDiscountService _employeeBenefitsDiscountService;
        private readonly IDependentBenefitsCostService _dependentBenefitsCostService;
        private readonly IDependentBenefitsDiscountService _dependentBenefitsDiscountService;

        public CostPreviewService(IEmployeePayService employeePayService,
            IEmployeeBenefitsCostService employeeBenefitsCostService,
            IEmployeeBenefitsDiscountService employeeBenefitsDiscountService,
            IDependentBenefitsCostService dependentBenefitsCostService,
            IDependentBenefitsDiscountService dependentBenefitsDiscountService)
        {
            _employeePayService = employeePayService;
            _employeeBenefitsCostService = employeeBenefitsCostService;
            _employeeBenefitsDiscountService = employeeBenefitsDiscountService;
            _dependentBenefitsCostService = dependentBenefitsCostService;
            _dependentBenefitsDiscountService = dependentBenefitsDiscountService;
        }

        public BenefitsCostPreviewModel CalculateBenefitsPreview(string employee, List<string> dependents)
        {
            var costPreview = new BenefitsCostPreviewModel();
            costPreview.Employee = employee;
            costPreview.BasePay = _employeePayService.GetEmployeePay(employee);
            
            var employeeCost = _employeeBenefitsCostService.GetEmployeeBenefitsCostPerPayPeriod(employee);
            costPreview.EmployeeDiscount = _employeeBenefitsDiscountService.GetEmployeeBenefitsDiscount(employee, employeeCost);
            costPreview.EmployeeBenefitsCost = employeeCost - costPreview.EmployeeDiscount;

            costPreview.DependentCostPreviews = new List<DependentCostPreviewModel>();
            
            if (dependents != null)
            {
                dependents.ForEach(dependent =>
                {
                    var dependentPreview = new DependentCostPreviewModel();

                    dependentPreview.Dependent = dependent;
                    var dependentCost = _dependentBenefitsCostService.GetDependentBenefitsCostPerPayPeriod(dependent);
                    dependentPreview.DependentDiscount = _dependentBenefitsDiscountService.GetDependentBenefitsDiscount(dependent, dependentCost);
                    dependentPreview.DependentBenefitsCost = dependentCost - dependentPreview.DependentDiscount;

                    costPreview.DependentCostPreviews.Add(dependentPreview);
                });
            }

            costPreview.BenefitsCostTotal = costPreview.EmployeeBenefitsCost + costPreview.DependentCostPreviews.Sum(preview => preview.DependentBenefitsCost);
            costPreview.PayAfterBenefits = costPreview.BasePay - costPreview.BenefitsCostTotal;

            return costPreview;
        }
    }
}
