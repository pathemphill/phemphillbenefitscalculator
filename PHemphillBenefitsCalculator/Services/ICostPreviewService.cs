﻿using PHemphillBenefitsCalculator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PHemphillBenefitsCalculator.Services
{
    public interface ICostPreviewService
    {
        BenefitsCostPreviewModel CalculateBenefitsPreview(string employee, List<string> dependents);
    }
}
