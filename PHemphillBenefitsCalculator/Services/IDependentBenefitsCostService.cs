﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PHemphillBenefitsCalculator.Services
{
    public interface IDependentBenefitsCostService
    {
        double GetDependentBenefitsCostPerPayPeriod(string dependent);
    }
}
