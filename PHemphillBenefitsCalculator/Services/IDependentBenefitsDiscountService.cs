﻿namespace PHemphillBenefitsCalculator.Services
{
    public interface IDependentBenefitsDiscountService
    {
        double GetDependentBenefitsDiscount(string dependent, double currentCost);
    }
}
