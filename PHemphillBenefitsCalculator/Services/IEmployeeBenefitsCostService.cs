﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PHemphillBenefitsCalculator.Services
{
    public interface IEmployeeBenefitsCostService
    {
        double GetEmployeeBenefitsCostPerPayPeriod(string employee);
    }
}
