﻿namespace PHemphillBenefitsCalculator.Services
{
    public interface IEmployeeBenefitsDiscountService
    {
        double GetEmployeeBenefitsDiscount(string employee, double currentCost);
    }
}
