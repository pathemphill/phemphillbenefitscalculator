﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PHemphillBenefitsCalculator.Services
{
    public interface IEmployeePayService
    {
        double GetEmployeePay(string employee);
    }
}
