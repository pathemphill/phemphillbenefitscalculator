﻿namespace PHemphillBenefitsCalculator.Services
{
    public class NameStartsWithADependentBenefitsDiscountService : IDependentBenefitsDiscountService
    {
        public double GetDependentBenefitsDiscount(string dependent, double currentCost)
        {
            if (dependent != null && dependent.StartsWith("A"))
            {
                return 0.10 * currentCost;
            }
            return 0.0;
        }
    }
}
