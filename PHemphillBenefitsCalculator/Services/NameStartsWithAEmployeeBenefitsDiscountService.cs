﻿namespace PHemphillBenefitsCalculator.Services
{
    public class NameStartsWithAEmployeeBenefitsDiscountService : IEmployeeBenefitsDiscountService
    {
        public double GetEmployeeBenefitsDiscount(string employee, double currentCost)
        {
            if (employee != null && employee.StartsWith("A"))
            {
                return 0.10 * currentCost;
            }
            return 0.0;
        }
    }
}
