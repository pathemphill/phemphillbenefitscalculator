﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PHemphillBenefitsCalculator.Services;

namespace PHemphillBenefitsCalculator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string AllowAngularOrigin = "_allowAngularOrigin";

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddSingleton<ICostPreviewService, CostPreviewService>();
            services.AddSingleton<IDependentBenefitsCostService, ConstantDependentBenefitsCostService>();
            services.AddSingleton<IDependentBenefitsDiscountService, NameStartsWithADependentBenefitsDiscountService>();
            services.AddSingleton<IEmployeeBenefitsCostService, ConstantEmployeeBenefitsCostService>();
            services.AddSingleton<IEmployeeBenefitsDiscountService, NameStartsWithAEmployeeBenefitsDiscountService>();
            services.AddSingleton<IEmployeePayService, ConstantEmployeePayService>();
            services.AddSingleton<IPayPeriodsService, TwoWeeksPayPeriodsService>();
            services.AddCors(options =>
            {
                options.AddPolicy(AllowAngularOrigin,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200");
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(AllowAngularOrigin);

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
