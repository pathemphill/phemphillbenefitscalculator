import { createAction, props, union } from "@ngrx/store";
import { CostPreviewRequestModel, CostPreviewModel } from "../models";

export const PREVIEW_REQUEST_ACTION = "[Benefits Preview Page] Request Preview";
export const PREVIEW_REQUEST_SUCCESS_ACTION = "[Benefits Preview Api] Request Success";
export const PREVIEW_REQUEST_FAILURE_ACTION = "[Benefits Preview Api] Request Failed";

export const previewRequestAction = createAction(
    PREVIEW_REQUEST_ACTION, 
    props<{requestModel: CostPreviewRequestModel}>());
export const previewRequestSuccessAction = createAction(
    PREVIEW_REQUEST_SUCCESS_ACTION,
    props<{preview: CostPreviewModel}>());
export const previewRequestFailedAction = createAction(PREVIEW_REQUEST_FAILURE_ACTION);

const all = union({
    previewRequestAction,
    previewRequestSuccessAction,
    previewRequestFailedAction,
})

export type PreviewPageActions = typeof all;
