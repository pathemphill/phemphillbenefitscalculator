import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PreviewContainerComponent } from "./components/preview-container/preview-container.component";


const routes: Routes = [
  {
    path: "",
    data: { readableName: "Benefits Cost Preview" },
    component: PreviewContainerComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
