import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule} from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { StoreModule } from "@ngrx/store";
import { reducers, metaReducers } from "./reducers";
import { EffectsModule } from "@ngrx/effects";
import { EmployeeInputComponent } from "./components/employee-input/employee-input.component";
import { CostPreviewComponent } from "./components/cost-preview/cost-preview.component";
import { PreviewContainerComponent } from "./components/preview-container/preview-container.component";
import { PreviewEffects } from "./effects/preview.effects";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    EmployeeInputComponent,
    CostPreviewComponent,
    PreviewContainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot([PreviewEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
