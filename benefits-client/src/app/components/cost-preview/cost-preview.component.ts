import { Component, OnInit, Input } from "@angular/core";
import { CostPreviewState } from 'src/app/reducers/cost-preview.reducer';

@Component({
  selector: "app-cost-preview",
  templateUrl: "./cost-preview.component.html",
  styleUrls: ["./cost-preview.component.css"]
})
export class CostPreviewComponent implements OnInit {
  @Input()
  previewState: CostPreviewState;

  constructor() { }

  ngOnInit() {
  }

}
