import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { EmployeeInputComponent } from "./employee-input.component";
import { FormGroup, Validators, FormArray, ReactiveFormsModule, FormControl } from '@angular/forms';

describe("EmployeeInputComponent", () => {
  let component: EmployeeInputComponent;
  let fixture: ComponentFixture<EmployeeInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeInputComponent ],
      imports: [ ReactiveFormsModule ]
    })
    .overrideComponent(EmployeeInputComponent, {
      set: {
        template: "",
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should build a form during init", () => {
    spyOn(component, "buildForm").and.returnValue(new FormGroup({}));

    component.ngOnInit();

    expect(component.buildForm).toHaveBeenCalled();
    expect(component.form).toBeTruthy();
  });

  it("should build a form for employee and dependents", () => {
    const result = component.buildForm();

    expect(result).toBeTruthy();
    expect(result.get("employee")).toBeTruthy();
    expect(result.get("employee").value).toBe("");
    expect(result.get("employee").validator).toBe(Validators.required);
    expect(result.get("dependents")).toBeTruthy();
    expect((result.get("dependents") as FormArray).length).toBe(0);
  });

  it("should add a control for a dependent to the form", () => {
    component.form = new FormGroup({
      dependents: new FormArray([]),
    });
    
    component.addDependent();

    const resultFormArray = component.form.get("dependents") as FormArray;
    expect(resultFormArray).toBeTruthy()
    expect(resultFormArray.length).toBe(1);
    expect(resultFormArray.at(0).value).toBe("");
    expect(resultFormArray.at(0).validator).toBe(Validators.required);
  });

  it("should remove a control for a dependent from the form", () => {
    component.form = new FormGroup({
      dependents: new FormArray([
        new FormControl("Bob", Validators.required),
        new FormControl("Sue", Validators.required),
      ]),
    });

    component.removeDependent(0);

    const resultFormArray = component.form.get("dependents") as FormArray;
    expect(resultFormArray).toBeTruthy()
    expect(resultFormArray.length).toBe(1);
    expect(resultFormArray.at(0).value).toBe("Sue");
    expect(resultFormArray.at(0).validator).toBe(Validators.required);
  });

  it("should trigger raise a preview request event", () => {
    spyOn(component.previewRequest, "next").and.stub();
    component.form = new FormGroup({
      employee: new FormControl("Joe", Validators.required),
      dependents: new FormArray([
        new FormControl("Bob", Validators.required),
        new FormControl("Sue", Validators.required),
      ]),
    });

    component.previewClicked();

    expect(component.previewRequest.next).toHaveBeenCalledWith(component.form.getRawValue());
  });

  it("should get the dependents FormArray", () => {
    const formArray = new FormArray([
      new FormControl("Bob", Validators.required),
      new FormControl("Sue", Validators.required),
    ]);
    component.form = new FormGroup({
      dependents: formArray,
    });

    expect(component.dependents).toBe(formArray);
  });
});
