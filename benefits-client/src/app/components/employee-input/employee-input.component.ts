import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CostPreviewRequestModel } from 'src/app/models';

@Component({
  selector: "app-employee-input",
  templateUrl: "./employee-input.component.html",
  styleUrls: ["./employee-input.component.css"]
})
export class EmployeeInputComponent implements OnInit {
  @Output()
  previewRequest = new EventEmitter<CostPreviewRequestModel>();

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.buildForm();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      employee: this.fb.control("", Validators.required),
      dependents: this.fb.array([]),
    });
  }

  addDependent() {
    this.dependents.push(this.fb.control("", Validators.required));
  }

  removeDependent(index: number) {
    this.dependents.removeAt(index);
  }

  previewClicked() {
    this.previewRequest.next(this.form.getRawValue() as CostPreviewRequestModel);
  }

  get dependents(): FormArray {
    return this.form.get("dependents") as FormArray;
  }

}
