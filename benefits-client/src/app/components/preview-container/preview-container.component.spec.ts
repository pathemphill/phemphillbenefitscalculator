import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { PreviewContainerComponent } from "./preview-container.component";
import { State } from 'src/app/reducers';
import { Store } from '@ngrx/store';
import { selectPreviewData } from 'src/app/selectors/preview-page.selectors';
import { CostPreviewRequestModel } from 'src/app/models';
import { previewRequestAction } from 'src/app/actions/preview-page.actions';

describe("PreviewContainerComponent", () => {
  let component: PreviewContainerComponent;
  let fixture: ComponentFixture<PreviewContainerComponent>;
  let storeStub: Store<State>;

  beforeEach(() => {
    storeStub = jasmine.createSpyObj(["select", "dispatch"]);
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviewContainerComponent ],
      providers: [
        {
          provide: Store,
          useValue: storeStub,
        }],
    }).overrideComponent(PreviewContainerComponent, {
      set: {
        template: "",
      }
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should select the cost preview from the store onInit", () => {
    component.ngOnInit();

    expect(storeStub.select).toHaveBeenCalledWith(selectPreviewData);
  });

  it("should dispatch a preview request", () => {
    component.handlePreviewRequest(new CostPreviewRequestModel());

    expect(storeStub.dispatch).toHaveBeenCalledWith(previewRequestAction({requestModel: new CostPreviewRequestModel()}));
  });
});
