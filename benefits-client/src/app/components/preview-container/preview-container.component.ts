import { Component, OnInit } from "@angular/core";
import { CostPreviewRequestModel, CostPreviewModel } from "src/app/models";
import { Store } from "@ngrx/store";
import * as PreviewPageActions from "../../actions/preview-page.actions";
import { Observable } from "rxjs";
import { selectPreviewData } from "src/app/selectors/preview-page.selectors";
import { State } from "src/app/reducers";
import { CostPreviewState } from "src/app/reducers/cost-preview.reducer";

@Component({
  selector: "app-preview-container",
  templateUrl: "./preview-container.component.html",
  styleUrls: ["./preview-container.component.css"]
})
export class PreviewContainerComponent implements OnInit {
  costPreview$: Observable<CostPreviewState>;

  constructor(private store: Store<State>) { }

  ngOnInit() {
    this.costPreview$ = this.store.select(selectPreviewData);
  }

  handlePreviewRequest(request: CostPreviewRequestModel) {
    this.store.dispatch(PreviewPageActions.previewRequestAction({requestModel: request}));
  }
}
