import { TestBed } from "@angular/core/testing";
import { provideMockActions } from "@ngrx/effects/testing";
import { Observable, of } from "rxjs";
import { PreviewEffects } from "./preview.effects";
import { CostPreviewApiService } from "../http/cost-preview-api.service";
import { CostPreviewModel, CostPreviewRequestModel } from "../models";
import { previewRequestAction, previewRequestSuccessAction, previewRequestFailedAction } from "../actions/preview-page.actions";

describe("PreviewEffects", () => {
  let actions$: Observable<any>;
  let effects: PreviewEffects;
  let costPreviewApiServiceStub: CostPreviewApiService;

  beforeEach(() => {
    
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PreviewEffects,
        provideMockActions(() => actions$),
        {
          provide: CostPreviewApiService,
          useValue: costPreviewApiServiceStub,
        }
      ]
    });

    effects = TestBed.get<PreviewEffects>(PreviewEffects);
  });

  it("should be created", () => {
    expect(effects).toBeTruthy();
  });

  it("should call to get a cost preview", () => {
    costPreviewApiServiceStub = {
      getCostPreview() { return of(new CostPreviewModel())}
    } as any as CostPreviewApiService;

    const requestModel = {
      employee: "Bob",
      dependents: ["Sue", "Joe"]
    } as CostPreviewRequestModel
    actions$ = of(previewRequestAction({requestModel: requestModel}));

    effects.getCostPreview$.subscribe(action => {
      expect(action).toEqual(previewRequestSuccessAction({preview: new CostPreviewModel()}));
    });
  });

  xit("should produce a failure action if api service errors", () => {
    costPreviewApiServiceStub = {
      getCostPreview() { throw new Error("Error"); }
    } as any as CostPreviewApiService;

    const requestModel = {
      employee: "Bob",
      dependents: ["Sue", "Joe"]
    } as CostPreviewRequestModel
    actions$ = of(previewRequestAction({requestModel: requestModel}));

    effects.getCostPreview$.subscribe(action => {
      expect(action).toEqual(previewRequestFailedAction());
    });
  });
});
