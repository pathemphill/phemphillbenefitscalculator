import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { CostPreviewApiService } from "../http/cost-preview-api.service";
import * as PreviewPageActions from "../actions/preview-page.actions"
import { map, catchError, switchMap } from "rxjs/operators";
import { of } from "rxjs";
import { CostPreviewModel } from "../models";

@Injectable()
export class PreviewEffects {
  getCostPreview$ = createEffect(() => this.actions$.pipe(
    ofType(PreviewPageActions.PREVIEW_REQUEST_ACTION),
    switchMap(action => this.costPreviewApiService.getCostPreview(action.requestModel).pipe(
      map((previewData: CostPreviewModel) => PreviewPageActions.previewRequestSuccessAction({preview: previewData})),
      catchError(() => of(PreviewPageActions.previewRequestFailedAction()))
    ))
  ));

  constructor(private actions$: Actions<PreviewPageActions.PreviewPageActions>,
    private costPreviewApiService: CostPreviewApiService) {}
}
