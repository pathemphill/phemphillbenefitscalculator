import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { CostPreviewApiService } from "./cost-preview-api.service";
import { CostPreviewRequestModel, CostPreviewModel } from "../models";
import { pipe } from "rxjs";
import { first, tap } from "rxjs/operators";

describe("CostPreviewApiService", () => {
  let httpMock: HttpTestingController;
  let service: CostPreviewApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(CostPreviewApiService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should make a preview get request to the backend", () => {
    const requestModel = {
      employee: "Bob",
    } as CostPreviewRequestModel;   

    const subscription = service.getCostPreview(requestModel).subscribe(pipe(first(), 
      tap(responseModel => expect(responseModel).toEqual(new CostPreviewModel()))));

    const request = httpMock.expectOne(`${service.costPreviewUrl}?employee=Bob`);
    expect(request.request.method).toBe("GET");
    
    request.flush(new CostPreviewModel());
    httpMock.verify();
    subscription.unsubscribe;
  });

  it("should build the appropriate query params for dependents", () => {
    const requestModel = {
      employee: "Bob",
      dependents: ["Sue", "Joe"]
    } as CostPreviewRequestModel;   

    const subscription = service.getCostPreview(requestModel).subscribe(pipe(first(), 
      tap(responseModel => expect(responseModel).toEqual(new CostPreviewModel()))));

    const request = httpMock.expectOne(`${service.costPreviewUrl}?employee=Bob&dependents=Sue&dependents=Joe`);
    expect(request.request.method).toBe("GET");
    
    request.flush(new CostPreviewModel());
    httpMock.verify();
    subscription.unsubscribe();
  });
});
