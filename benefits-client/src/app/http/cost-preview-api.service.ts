import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http"
import { CostPreviewRequestModel } from "../models";

@Injectable({
  providedIn: "root"
})
export class CostPreviewApiService {
  public costPreviewUrl = "https://localhost:44348/api/BenefitsPreview";

  constructor(private http: HttpClient) { }

  getCostPreview(request: CostPreviewRequestModel) {
    let params = new HttpParams().append("employee", request.employee);

    if(request.dependents) {
      request.dependents.forEach(dep => params = params.append("dependents", dep));
    }
    
    return this.http.get(this.costPreviewUrl, {params: params});
  }
}
