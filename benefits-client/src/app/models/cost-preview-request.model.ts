export class CostPreviewRequestModel {
    employee: string;
    dependents: Array<string>;
}
