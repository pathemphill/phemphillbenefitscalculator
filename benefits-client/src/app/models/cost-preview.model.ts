import { DependentCostPreview } from "./dependent-cost-preview.model";

export class CostPreviewModel {
    employee: string;
    basePay: number;
    employeeDiscount: number;
    employeeBenefitsCost: number;
    benefitsCostTotal: number;
    payAfterBenefits: number;
    dependentCostPreviews: Array<DependentCostPreview>;
}
