export class DependentCostPreview {
    dependent: string;
    dependentDiscount: number;
    dependentBenefitsCost: number;
}
