import * as PreviewReducer from "./cost-preview.reducer";
import { previewRequestFailedAction, previewRequestSuccessAction } from "../actions/preview-page.actions";
import { CostPreviewModel } from "../models";

describe("Cost Preview Reducer", () => {
    it("should have an 'empty' initial state", () =>  {
        const state = PreviewReducer.initialState;

        expect(state.previewData).toBeNull();
        expect(state.errored).toBe(false);
    });

    it("should update state from a request success action", () => {
        const preview = new CostPreviewModel();

        const newState = PreviewReducer.reducer(PreviewReducer.initialState, previewRequestSuccessAction({preview: preview}));

        expect(newState.previewData).toBe(preview);
        expect(newState.errored).toBe(false);
    });

    it("should update state from a request failed action", () => {
        const newState = PreviewReducer.reducer(PreviewReducer.initialState, previewRequestFailedAction());

        expect(newState.previewData).toBeNull();
        expect(newState.errored).toBe(true);
    });
});
