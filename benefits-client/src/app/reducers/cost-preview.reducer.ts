import { createReducer, on, Action } from "@ngrx/store";
import * as PreviewPageActions from "../actions/preview-page.actions";
import { CostPreviewModel } from "../models";

export interface CostPreviewState {
    previewData: CostPreviewModel;
    errored: boolean;
}

export const initialState: CostPreviewState = {
    previewData: null,
    errored: false,
}

const previewReducer = createReducer(
    initialState,
    on(PreviewPageActions.previewRequestSuccessAction, (state, { preview }) => ({ ...state, previewData: preview, errored: false})),
    on(PreviewPageActions.previewRequestFailedAction, state => ({ ...state, errored: true }))
);

export function reducer(state: CostPreviewState | undefined, action: Action) {
    return previewReducer(state, action);
}
