import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from "@ngrx/store";
import { environment } from "../../environments/environment";
import { CostPreviewModel } from "../models";
import * as fromPreview from "./cost-preview.reducer"

export interface State {
  preview: fromPreview.CostPreviewState;
}

export const reducers: ActionReducerMap<State> = {
  preview: fromPreview.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
