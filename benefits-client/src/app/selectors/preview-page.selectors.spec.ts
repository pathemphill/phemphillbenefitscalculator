import { State } from "../reducers";
import { CostPreviewModel } from "../models";
import { selectPreviewData } from "./preview-page.selectors";

describe("Preview Page Selectors", () => {
    it("should select the preview data from state", () => {
        const state = {
            preview: {
                previewData: new CostPreviewModel(),
                errored: false,
            }
        } as State;

        const selectedState = selectPreviewData(state);

        expect(selectedState).toBe(state.preview);
    })
});
