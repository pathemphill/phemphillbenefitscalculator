import { State } from "../reducers";

export const selectPreviewData = (state: State) => state.preview;